class ProductsController < ApplicationController

  def index
    @products = HTTParty.get("http://localhost:3000/search/#{params[:q]}") if params[:q].present?
  end

end
